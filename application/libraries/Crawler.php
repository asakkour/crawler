<?php 

/*
Github: https://github.com/Lukeas14/codeigniter_crawler
Author: Justin Lucas (Lukeas14@gmail.com)
Copyright (c) 2012 Justin Lucas
Licensed under MIT License (https://github.com/jquery/jquery/blob/master/MIT-LICENSE.txt)
*/

require_once('Simple_html_dom.php');

class Crawler{
	protected $site_link;

	public function setLink($site_link)
	{
		$this->site_link = $site_link;
	}

	// return links web site
	public function getLinks()
	{
		$html   = new simple_html_dom();
		$http   = "http://";
	    $https  = "https://";
	    $links  = [];
	    $result = [];
	    if(strpos($this->site_link,$http)!== false 
	    	|| strpos($this->site_link,$https)!== false){
	    	$html->load_file($this->site_link);
	    	foreach($html->find('a') as $value)
			{
				if(!empty($value->href) && $value->href != "#")
				{
					$url = trim(trim($value->href, "/"));
					if(!in_array($url, $links))
					{
						array_push($links, $url);
					}
				}
			}

			foreach ($links as $url) {
				array_push($result, [
					'url'    => $url,
					'statut' => $this->getLinkStatut($url)
				]);
			}
	    }

		return $result;
	}

	// function for get link statut
	public function getLinkStatut($url)
	{
		$headers = @get_headers( $url);
		$headers = (is_array($headers)) ? implode( "\n ", $headers) : $headers;

		if((bool)preg_match('#^HTTP/.*\s+[(200|301|302)]+\s#i', $headers))
			return 1;
		else
			return 0;
	}
}