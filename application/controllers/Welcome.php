<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	
	public function index()
	{
		if($this->input->post('link')){
			// validation
			$this->load->helper(array('form', 'url'));
			$this->load->library('form_validation');
			if ($this->form_validation->run() == FALSE)
            {
                $url   = $this->input->post('link');
				$links = $this->getLinksFromUrl($url);
				$this->load->helper('url');
				$this->load->view('welcome_message', ['links'=>$links, 'url' => $url]);
            }
		}else{
			$this->load->view('welcome_message');
		}
	}


	public function export()
	{
		$data = $this->getLinksFromUrl($this->input->get('url'));
		header("Content-type: application/csv");
        header("Content-Disposition: attachment; filename=\"test".".csv\"");
        header("Pragma: no-cache");
        header("Expires: 0");
        $handle = fopen('php://output', 'w');
        foreach ($data as $data) {
            fputcsv($handle, $data);
        }
            fclose($handle);
        exit;
	}

	public function getLinksFromUrl($link)
	{
		$this->load->library('crawler');
		$this->crawler->setLink($link);
		return $this->crawler->getLinks();
	}
}
