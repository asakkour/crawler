<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to CodeIgniter</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
	<style type="text/css">
	::selection { background-color: #E13300; color: white; }
	::-moz-selection { background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body {
		margin: 0 15px 0 15px;
	}

	p.footer {
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}

	#container {
		margin: 10px;
		border: 1px solid #D0D0D0;
		box-shadow: 0 0 8px #D0D0D0;
	}
	</style>
</head>
<body>

<div id="container">
	<h1>WEB CRAWLER</h1>

	<div id="body" class="container">
		<form method="post" class="row">
			<div class="col-lg-8 col-lg-offset-2">
				<div class="form-group">
				    <input type="url" class="form-control" name="link" id="link" placeholder="URL : ">
				</div>
			</div>
			<div class="col-lg-4">
				<div class="form-group">
		    		<button type="submit" class="btn btn-primary form-control">Crawler</button>
				</div>
			</div>
		</form>
		<br>
		<?php if (!empty($links)): ?>
			<div class="row">
				<div class="col-lg-4">
					<a class="btn btn-success" target="_black" href="<?=site_url('welcome/export'); ?>?url=<?=$url ?>">
						Export to CSV
					</a>
				</div>
			</div>
			<br><br>
			<table class="table">
			  <thead>
			    <tr>
			      <th scope="col">#</th>
			      <th scope="col">Link</th>
			      <th scope="col">Statut</th>
			    </tr>
			  </thead>
			  <tbody>
			  	  <?php $i = 1; ?>
				  <?php foreach ($links as $link): ?>
				    <tr>
				      <th scope=><?=$i ?></th>
				      <td><?=$link['url'] ?></td>
				      <td><?=$link['statut'] ?></td>
				      <?php $i++; ?>
				    </tr>
				  <?php endforeach ?>
			  </tbody>
			</table>
		<?php endif ?>
	</div>
</div>



</body>
</html>